const Materia = require('../models/Materia');

const create = (req, res) =>{
    let materia = new Materia();

    materia.codigo = req.body.codigo;
    materia.nombre= req.body.nombre;
    materia.curso= req.body.curso;
    materia.horas= req.body.horas;

    materia.save(function (err,data) {
        if (err) return handleError(err);
        res.json(data);
      });
}
const show = (req, res) =>{
    Materia.findById(req.params.id, function(err, materia) {
        res.json(materia);
    })
}
const remove = (req, res) =>{
    Materia.findById(req.params.id, function(err, materia) {
        if (err) return res.json({"error": true, "message": "Error borrando la materia"});
        if(materia==null) return res.json({"error": true, "message": "Error borrando la materia"});
        Materia.remove({_id: req.params.id}, function(err) {
            if (err) {
              return res.json({"error": true, "message": "Error borrando la materia"});
            }else{
                return res.json({"Correcto": true, "message": "Materia Borrada Correctamente"});
            }
        })
    })
}
const update = (req, res) => {
    Materia.findOne({
        _id: req.params.id
      })
      .then((materia) => {
        materia.codigo = req.body.codigo;
        materia.nombre= req.body.nombre;
        materia.curso= req.body.curso;
        materia.horas= req.body.horas;
        materia
          .save()
          .then(() => {
            res.jsonp({ materia });
          });
      });
  }
  const list = (req, res) => {
      Materia.find((err, materias) => {
        if (err) {
          return res.status(500).json({
            message: 'Error obteniendo las materias'
          })
        }
        return res.json(materias)
      })
    }
module.exports ={
    create,
    show,
    update,
    remove,
    list
}