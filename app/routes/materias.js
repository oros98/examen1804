var router = require('express').Router()
var materiaController = require ('../controllers/materiaController')
const auth = require('../middlewares/auth')

router.get('/privada', (req, res) => {
    auth.auth(req,res,function(){
        materiaController.list(req, res)

    })
})
router.get('/', (req, res) => {
    materiaController.list(req, res)
})
router.get('/:id', (req, res) => {
    materiaController.show(req, res)
})
router.post('/', (req, res) => {
    materiaController.create(req, res)
})
router.put('/:id', (req, res) => {
    materiaController.update(req, res)
})
router.delete('/:id', (req, res) => {
    materiaController.remove(req, res)
})
module.exports = router